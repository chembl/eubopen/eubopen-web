import * as d3 from 'd3'

const getSunburstColorInterpolator = () => {
  return d3.interpolateHcl('#113a70', '#95CEBD')
}

export default {
  getSunburstColorInterpolator,
}
