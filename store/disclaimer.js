export const state = () => ({
  showDisclaimerDialog: true,
})

export const mutations = {
  SET_SHOW_DISCLAIMER_DIALOG(state, showDisclaimerDialog) {
    state.showDisclaimerDialog = showDisclaimerDialog
  },
}

export const actions = {
  setShowDisclaimer({ commit, state, dispatch }, show) {
    commit('SET_SHOW_DISCLAIMER_DIALOG', show)
  },
}
