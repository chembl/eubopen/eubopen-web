import colors from 'vuetify/es5/util/colors'
import AllCompoundIDs from './sitemap/AllCompoundIDs.json'
import AllTargetIDs from './sitemap/AllTargetIDs.json'
require('dotenv').config({ path: process.env.ENV_FILE_PATH })

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - EUbOPEN',
    title: 'EUbOPEN Portal',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'The EUbOPEN consortium is an Innovative Medicines Initiative (IMI) funded project to enable and unlock biology in the open.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  env: {
    // DEV or PROD
    enviroment: process.env.ENVIRONMENT || 'DEV',
    esProxyBaseUrl:
      process.env.ES_PROXY_BASE_URL ||
      'https://www.ebi.ac.uk/chembl/interface_api/es_proxy',
    delayedJobsBaseUrl:
      process.env.DELAYED_JOBS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/interface_api/delayed_jobs',
    chemblWSBaseUrl:
      process.env.CHEMBL_WS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/api/data',
    fallbackIMGsBaseUrl:
      process.env.FALLBACK_IMGS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/k8s/static/chembl/img',
    delayedJobsIgnoreCache: process.env.DELAYED_JOBS_IGNORE_CACHE === 'true',
    esIndexPrefix: process.env.ES_INDEX_PREFIX || 'chembl_',
    djJobStatusCheckIntervalMillis: process.env
      .DJ_JOB_STATUS_CHECK_INTERVAL_MILLIS
      ? parseInt(process.env.DJ_JOB_STATUS_CHECK_INTERVAL_MILLIS)
      : 2000,
    compoundReportCardsBaseUrl:
      process.env.COMPOUND_REPORT_CARDS_BASE_URL || '/compound',
    targetReportCardsBaseUrl:
      process.env.TARGET_REPORT_CARDS_BASE_URL || '/target',
    cellLineReportCardsBaseUrl:
      process.env.CELL_LINE_REPORT_CARDS_BASE_URL || '/cell_line_report_card',
    eubopenCompoundReportCardsBaseUrl:
      process.env.EUBOPEN_COMPOUND_REPORT_CARDS_BASE_URL || '/compound',
    eubopenTargetReportCardsBaseUrl:
      process.env.EUBOPEN_TARGET_REPORT_CARDS_BASE_URL || '/target',
    eubopenAssayReportCardsBaseUrl:
      process.env.EUBOPEN_ASSAY_REPORT_CARDS_BASE_URL || '/assay',
    eubopenTextSearchBaseUrl:
      process.env.EUBOPEN_TEXT_SEARCH_BASE_URL || '/search',
    assayReportCardsBaseUrl:
      process.env.ASSAY_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/assay_report_card',
    documentReportCardsBaseUrl:
      process.env.ASSAY_REPORT_CARDS_BASE_URL ||
      'https://wwwdev.ebi.ac.uk/chembl/document_report_card',
    entityBrowserStateUrlTemplate:
      process.env.ENTITY_BROWSER_STATE_URL_TEMPLATE ||
      '/<BROWSER_NAME>/browse/<GENERATED_STATE>',
    entityBrowserStateUrlTemplateNew:
      process.env.ENTITY_BROWSER_STATE_URL_TEMPLATE_NEW ||
      '/<BROWSER_NAME>/browse/<GENERATED_STATE>',
    canonicalDomain: process.env.CANONICAL_DOMAIN || '0.0.0.0:3000',
    GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
    esDataMaxURLLength: 4094,
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~/plugins/vue-apexchart.js', ssr: false }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/google-gtag',
    '@nuxtjs/sitemap',
  ],

  'google-gtag': {
    id: process.env.GOOGLE_ANALYTICS_ID, // required
    debug: false,
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#113a70',
          accent: '#E4EAF3',
          secondary: '#32715D',
          info: '#96cfbd',
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent4,
        },
      },
    },
  },
  sitemap() {
    const compoundsRoutes = AllCompoundIDs.ids.map((id) => `/compound/${id}`)
    const targetsRoutes = AllTargetIDs.ids.map(
      (id) => `/target/${id.replace(/\//g, '___')}`
    )
    const routes = [...compoundsRoutes, ...targetsRoutes]
    return {
      routes,
      hostname: 'https://gateway.eubopen.org/',
      gzip: true,
    }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      '@chembl/chembl-elasticsearch-service',
      '@chembl/metadata-tags-generator',
    ],
  },
}
