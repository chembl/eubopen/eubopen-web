FROM node:16.18.0
ENV ENV_FILE_PATH=${ENV_FILE_PATH:-'/etc/run_config/.env'}
ARG BUILD_ENV_FILE_PATH=${BUILD_ENV_FILE_PATH:-'/etc/run_config/.env'}

LABEL maintainer="dmendez@ebi.ac.uk"

ENV APP_SOURCE /usr/src/eubopen-web

RUN mkdir -p ${APP_SOURCE}
WORKDIR ${APP_SOURCE}

RUN apt-get update -qq -y && \
    apt-get upgrade -qq -y && \
    apt-get install -qq -y git 

COPY . ${APP_SOURCE}

RUN npm install
RUN ENV_FILE_PATH="${APP_SOURCE}/${BUILD_ENV_FILE_PATH}" npm run build

ENV HOST 0.0.0.0

EXPOSE 3000

ENTRYPOINT npm start
