import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import Summary from '~/components/report_cards/assay/Summary.vue'

const methods = {
  generateReportCardStructure(itemID, description) {
    return {
      title: description,
      entityName: EntityNames.EubopenAssay.singularEntityName,
      sections: [
        {
          id: 'Summary',
          title: 'Summary',
          component: Summary,
          index: 0,
        },
      ],
    }
  },
}

export default methods
