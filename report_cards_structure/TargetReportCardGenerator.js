import NameAndClassification from '~/components/report_cards/target/NameAndClassification.vue'
import ActivityCharts from '~/components/report_cards/target/ActivityCharts/ActivityCharts.vue'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import TargetCrystalStructures from '~/components/report_cards/target/TargetCrystalStructures.vue'
import ProbesAndChemogenomicCompounds from '~/components/report_cards/target/ProbesAndChemogenomicCompounds.vue'
import MainCompounds from '~/components/report_cards/target/MainCompounds.vue'

const methods = {
  generateReportCardStructure(itemID, prefName) {
    return {
      title: prefName,
      entityName: EntityNames.EubopenTarget.singularEntityName,
      sections: [
        {
          id: 'NameAndClassification',
          title: 'Name and Classification',
          component: NameAndClassification,
        },
        {
          id: 'ProbesAndChemogenomicCompounds',
          title: 'Probes and Chemogenomic Compounds',
          component: ProbesAndChemogenomicCompounds,
        },
        {
          id: 'MainCompounds',
          title: 'Main Compounds',
          component: MainCompounds,
        },
        {
          id: 'ActivityCharts',
          title: 'Compound Activity',
          component: ActivityCharts,
        },
        {
          id: 'CrystalStructures',
          title: 'Uniprot Accessions',
          component: TargetCrystalStructures,
        },
      ],
    }
  },
}

export default methods
