import Summary from '~/components/report_cards/chemical_probe/Summary/Summary.vue'
import ControlStructuresAndUse from '~/components/report_cards/chemical_probe/ControlStructuresAndUse.vue'
import ProbesForThisCompound from '~/components/report_cards/chemical_probe/ProbesForThisCompound.vue'
import QualityControl from '~/components/report_cards/chemical_probe/QualityControl.vue'
import CompoundActivityProfile from '~/components/report_cards/chemical_probe/CompoundActivityProfile.vue'
import CompoundCrystalStructures from '~/components/report_cards/chemical_probe/CompoundCrystalStructures.vue'
import CalculatedProperties from '~/components/report_cards/chemical_probe/CalculatedProperties.vue'
import EntityNames from '~/web-components-submodule/standardisation/EntityNames.js'
import CellViabilityAndHealthData from '~/components/report_cards/chemical_probe/CellViabilityAndHealthData.vue'
import MainTargets from '~/components/report_cards/chemical_probe/MainTargets.vue'

const methods = {
  generateReportCardStructure({
    itemID,
    chemblID,
    prefName,
    isChemicalProbe,
    isNegativeControl,
    showCellHeatlhAndViabilityData,
    showQualityControlData,
    showActivityProfileData,
  }) {
    const basePageStructure = [
      {
        id: 'Summary',
        title: 'Summary',
        component: Summary,
        include: true,
      },

      {
        id: 'MainTargets',
        title: 'Main Targets',
        component: MainTargets,
        componentParams: {
          chemblID,
        },
        include: true,
      },
      {
        id: 'ControlStructuresAndUse',
        title: 'Negative Controls',
        component: ControlStructuresAndUse,
        include: isChemicalProbe,
      },
      {
        id: 'ProbesForThisCompound',
        title: 'Related Probes',
        component: ProbesForThisCompound,
        include: isNegativeControl,
      },
      {
        id: 'QualityControl',
        title: 'Quality Control',
        component: QualityControl,
        componentParams: {
          chemblID,
        },
        include: showQualityControlData,
      },
      {
        id: 'ActivityProfile',
        title: 'Activity Profile',
        component: CompoundActivityProfile,
        componentParams: {
          chemblID,
        },
        include: showActivityProfileData,
      },
      {
        id: 'CellViabilityAndHealthData',
        title: 'Cell Health Data',
        component: CellViabilityAndHealthData,
        componentParams: {
          chemblID,
        },
        include: showCellHeatlhAndViabilityData,
      },
      {
        id: 'CalculatedProperties',
        title: 'Calculated Properties',
        component: CalculatedProperties,
        componentParams: {
          entityID: EntityNames.EubopenCompound.entityID,
        },
        include: true,
      },
      {
        id: 'CrystalStructures',
        title: 'PDB Ligand Codes',
        component: CompoundCrystalStructures,
        componentParams: {
          chemblID,
        },
        include: true,
      },
    ]

    const finalPageStructure = {
      title: prefName,
      entityName: EntityNames.EubopenCompound.singularEntityName,
      sections: [],
    }

    let index = 0

    for (const section of basePageStructure) {
      if (section.include) {
        section.index = index
        finalPageStructure.sections.push(section)
        index++
      }
    }

    return finalPageStructure
  },
}

export default methods
